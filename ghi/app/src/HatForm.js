import React from 'react';

class HatForm extends React.Component {
    constructor() {
        super()
        this.state = {
            fabric: '',
            styleName: '',
            color: '',
            URL: '',
            locations: []
          };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleURLChange = this.handleURLChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);

    }
    handleFabricChange(event) {
    const value = event.target.value;
    this.setState({'fabric': value})
}

    handleStyleNameChange(event) {
    const value = event.target.value;
    this.setState({'styleName': value})
}

    handleColorChange(event) {
    const value = event.target.value;
    this.setState({'color': value})
}

    handleLocationChange(event) {
    const value = event.target.value;
    this.setState({'location': value})
}
    handleURLChange(event) {
    const value = event.target.value;
    this.setState({'URL': value})
}
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.style_name = data.styleName
        delete data.styleName;
        delete data.locations;
        console.log(data);

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
                const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                fabric: '',
                styleName: '',
                color: '',
                URL: '',
                location: '',
              };

              this.setState(cleared);

        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({'locations': data.locations});

        }
    }

    render() {
        return (
            <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="name">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"/>
                  <label htmlFor="style_name">Style name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleURLChange} value={this.state.URL} placeholder="URL" required type="text" name="url" id="url" className="form-control"/>
                  <label htmlFor="url">URL</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleLocationChange} value={this.state.location} name="location" id="location" className="form-select">
                    <option>Choose a location</option>
                    {this.state.locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                                {location.closet_name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
        );
    }
}
export default HatForm;
